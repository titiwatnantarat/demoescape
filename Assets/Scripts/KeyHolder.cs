using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyHolder : MonoBehaviour
{
  
  
   private List<KeyDoor.KeyType> keyList;

   private void Awake() 
   {
        keyList = new List<KeyDoor.KeyType>();
    
   }

   public void AddKey(KeyDoor.KeyType keyType)
   {
    Debug.Log("Add key" + keyType);
    keyList.Add(keyType);
   }

   public void RemoveKey(KeyDoor.KeyType keyType)
   {
    keyList.Remove(keyType);
   }

   public bool ContainsKey(KeyDoor.KeyType keyType)
   {
    return keyList.Contains(keyType);
   }

   private void OnTriggerEnter(Collider collider)
   {
    KeyDoor keyDoor = collider.GetComponent<KeyDoor>();
    if(keyDoor != null)
    {
        AddKey(keyDoor.GetKeyType());
        Destroy(keyDoor.gameObject);
    }
    Door Door = collider.GetComponent<Door>();
    if(Door != null)
    {
        if(ContainsKey(Door.GetKeyType()))
        {
            RemoveKey(Door.GetKeyType());
            Door.OpenDoor();
        }
    }
   }

}
