using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    // [SerializeField] private Animator myDoor = null;
    [SerializeField] private KeyDoor.KeyType keyType;
    // [SerializeField] private bool openTrigger = false;
    // [SerializeField] private bool closeTrigger = false; 

    // [SerializeField] private string doorOpen = "DoorOpen";
    // [SerializeField] private string doorClose = "DoorClose";

    // public GameObject player;

    // private void OnTriggerEnter(Collider other)
    // {
    //     if(other.gameObject == player)
    //     {
    //         myDoor.Play(doorOpen ,0,0,0f);
    //         gameObject.SetActive(false);

    //     }
    //     else if(closeTrigger)

    //     {
    //         myDoor.Play(doorClose,0,0,0f);
    //         gameObject.SetActive(false);
    //     }
    // }    

    public KeyDoor.KeyType GetKeyType()
    {
        return keyType;
    }


    public void OpenDoor()
    {
         
        gameObject.SetActive(false);
    }
   
}
