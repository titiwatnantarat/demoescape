using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Volume : MonoBehaviour
{
    [SerializeField]Slider volumeSlider;
    
    public int volume;
    void Start()
    {
        
        if(!PlayerPrefs.HasKey("musicVolume"))
        {
            PlayerPrefs.SetFloat("musicVolume" , 1);
        }else
        {
            Load();
        }
    }

   
   public void ChangVolume()
   {
        AudioListener.volume = volumeSlider.value;
        Save();
   }

   public void Load()
   {
        volumeSlider.value = PlayerPrefs.GetFloat("musicVolume");
   }

   public void Save()
   {
        PlayerPrefs.SetFloat("musicVolume", volumeSlider.value);
   }
}
