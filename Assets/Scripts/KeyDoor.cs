using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyDoor : MonoBehaviour
{
    [SerializeField] private KeyType keyType;
   
   public enum KeyType
   {
    Gold,
    Sliver,
    White
   }

   public KeyType GetKeyType()
   {
    return keyType;
   }
}
